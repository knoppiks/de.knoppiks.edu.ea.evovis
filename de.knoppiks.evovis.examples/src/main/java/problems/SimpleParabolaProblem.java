package problems;

import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.EventBus;
import de.knoppiks.evovis.model.*;
import de.knoppiks.evovis.model.EvolutionaryAlgorithm.PopulationCounter;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.google.common.collect.Lists.newArrayList;
import static de.knoppiks.evovis.model.Mutators.mutateAll;
import static de.knoppiks.evovis.model.RatingFunctions.unlimited;
import static de.knoppiks.evovis.model.Selectors.all;
import static de.knoppiks.evovis.model.Selectors.tournamentSelect;
import static de.knoppiks.evovis.model.EnvironmentalSelectors.both;
import static java.util.stream.Collectors.toList;

public class SimpleParabolaProblem implements EvolutionaryProblem<Double> {

    private static final EventBus EVENT_BUS = new EventBus();
    private final Random r = new Random(1337);

    public static void main(String[] args) throws RatingFunction.RatingsExhaustedException {
        EvolutionaryAlgorithm algorithm = new EvolutionaryAlgorithm(EVENT_BUS);
        PopulationCounter counter = new PopulationCounter();
        EVENT_BUS.register(counter);

        Optional<Individual<Double>> best = algorithm.run(new SimpleParabolaProblem());

        System.out.println("after " + counter.totalPopulations() + " iterations the best result was: " + best.get());
    }

    private static Collection<Individual<Double>> recombine(Collection<Individual<Double>> population) {
        List<Individual<Double>> newPopulation = newArrayList(population);
        for (Individual<Double> x : population) {
            newPopulation.addAll(population
                    .stream()
                    .map(y -> (x.getValue() + y.getValue()))
                    .map(y -> y / 2.0)
                    .map(y -> new Individual<>(y))
                    .collect(toList()));
        }
        return population;
    }

    @Override
    public Collection<Double> getStartPopulation() {
        return ImmutableList.of(10.0, 9.0, -8.0, 12.0, -20.0);
    }

    @Override
    public RatingFunction<Double> getRater() {
        return RatingFunctions.unlimited(i -> -(i * i));
    }

    @Override
    public EnvironmentalSelector<Double> getEnvironmentalSelector() {
        return EnvironmentalSelectors.both(Selectors.tournamentSelect(r, 10, 5));
    }

    @Override
    public Mutator<Double> getMutator() {
        return Mutators.mutateAll(p -> p + r.nextGaussian());
    }

    @Override
    public Selector<Double> getParentSelector() {
        return Selectors.all();
    }

    @Override
    public Recombinator<Double> getRecombinator() {
        return p -> recombine(p);
    }

    @Override
    public TerminationCondition<Double> getCondition() {
        return (best, popNum) -> Math.abs(best.getFitness().get()) <= 1.0E-5;
    }
}
