package model;

import de.knoppiks.evovis.model.RatingFunction;
import org.junit.Test;

import static de.knoppiks.evovis.model.RatingFunctions.cached;
import static de.knoppiks.evovis.model.RatingFunctions.unlimited;
import static org.junit.Assert.assertEquals;

public class RatingFunctionsTest {

    @Test
    public void cachedTimesCalledInitNull() throws Exception {
        RatingFunction<Double> r = cached(unlimited((Double a) -> a));

        assertEquals(r.timesCalled(), 0);
    }

    @Test
    public void cachedTimesCalledAfterOneRatingIsOne() throws Exception {
        RatingFunction<Double> r = cached(unlimited((Double a) -> a));

        assertEquals(r.rate(2.0), 2.0, 0.0);
        assertEquals(r.timesCalled(), 1);
    }

    @Test
    public void cachedTimesCalledAfterTwoEqualRatingsIsOne() throws Exception {
        RatingFunction<Double> r = cached(unlimited((Double a) -> a));

        assertEquals(r.rate(2.0), 2.0, 0.0);
        assertEquals(r.rate(2.0), 2.0, 0.0);
        assertEquals(r.timesCalled(), 1);
    }

    @Test
    public void cachedTimesCalledAfterTwoDifferentCallsIsTwo() throws Exception {
        RatingFunction<Double> r = cached(unlimited((Double a) -> a));

        assertEquals(r.rate(2.0), 2.0, 0.0);
        assertEquals(r.rate(1.0), 1.0, 0.0);
        assertEquals(r.timesCalled(), 2);
    }
}