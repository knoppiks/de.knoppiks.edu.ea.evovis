package de.knoppiks.evovis.model;

import com.google.common.collect.ImmutableList;

public abstract class EnvironmentalSelectors {

    public static <T> EnvironmentalSelector<T> both(Selector<T> real) {
        return (p, o) -> real.select(ImmutableList.<Individual<T>>builder().addAll(p).addAll(o).build());
    }

    public static <T> EnvironmentalSelector<T> offspringOnly(Selector<T> real) {
        return (p, o) -> real.select(o);
    }
}
