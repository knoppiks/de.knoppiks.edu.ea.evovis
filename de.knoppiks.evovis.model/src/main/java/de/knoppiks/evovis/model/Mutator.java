package de.knoppiks.evovis.model;

import java.util.Collection;

public interface Mutator<T> {

    Collection<Individual<T>> mutate(Collection<Individual<T>> population);
}
