package de.knoppiks.evovis.model;

import java.util.Collection;

public interface Recombinator<T> {

    Collection<Individual<T>> recombine(Collection<Individual<T>> population);
}
