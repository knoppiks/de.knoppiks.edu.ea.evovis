package de.knoppiks.evovis.model;

import java.util.Iterator;
import java.util.function.Function;

import static com.google.common.collect.ImmutableSet.copyOf;
import static com.google.common.collect.Iterables.concat;
import static java.util.stream.Collectors.toList;

public abstract class Mutators {
    public static <T> Mutator<T> mutateAll(Function<T, T> singleMutation) {
        return p -> copyOf(concat(copyOf(p), p
                .stream()
                .map(i -> new Individual<T>(singleMutation.apply(i.getValue())))
                .collect(toList())));
    }

    public static <T> Mutator<T> mutateRandom(Function<T, T> singleMutation,
                                              double rate,
                                              Iterator<Double> randoms) {
        return p -> copyOf(concat(copyOf(p), p
                .stream()
                .map(i -> {
                    if (randoms.next() < rate) {
                        return new Individual<>(singleMutation.apply(i.getValue()));
                    } else {
                        return i;
                    }
                })
                .collect(toList())));
    }
}
