package de.knoppiks.evovis.model;

import de.knoppiks.evovis.model.RatingFunction.RatingsExhaustedException;

public interface TerminationCondition<T> {

    /**
     * Returns {@code true} if the condition for termination is met. This is the indicator for the evolutionary
     * algorithm to terminate. Note that the evolutionary algorithm may also stop if the {@link RatingFunction} throws
     * a {@link RatingsExhaustedException}.
     *
     * @param bestIndividual The best individual the algorithm could produce in the current population
     * @param iterations     The number of the current population
     * @return {@code true} if the result is satisfying
     */
    boolean met(Individual<T> bestIndividual, int iterations);
}
