package de.knoppiks.evovis.model;


import java.util.Collection;

public interface EvolutionaryProblem<T> {
    Collection<T> getStartPopulation();

    RatingFunction<T> getRater();

    EnvironmentalSelector<T> getEnvironmentalSelector();

    Mutator<T> getMutator();

    Selector<T> getParentSelector();

    Recombinator<T> getRecombinator();

    TerminationCondition<T> getCondition();
}
