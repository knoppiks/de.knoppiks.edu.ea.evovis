package de.knoppiks.evovis.model;

import java.util.Collection;

public interface EnvironmentalSelector<T> {

    Collection<Individual<T>> select(Collection<Individual<T>> parents, Collection<Individual<T>> offspring);
}
