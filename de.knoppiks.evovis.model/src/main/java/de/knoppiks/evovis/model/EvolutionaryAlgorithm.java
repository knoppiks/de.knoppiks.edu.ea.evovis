package de.knoppiks.evovis.model;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.knoppiks.evovis.model.RatingFunction.RatingsExhaustedException;

import java.util.Collection;
import java.util.Optional;

import static com.google.common.collect.ImmutableList.copyOf;
import static de.knoppiks.evovis.model.Individual.FITNESS_ORDERING;
import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.toList;

public class EvolutionaryAlgorithm {

    private final EventBus eventBus;

    public EvolutionaryAlgorithm(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public <T> Optional<Individual<T>> run(EvolutionaryProblem<T> p) throws RatingsExhaustedException {
        return this.run(p.getStartPopulation(), p.getRater(), p.getEnvironmentalSelector(), p.getMutator(),
                p.getParentSelector(), p.getRecombinator(), p.getCondition());
    }

    public <T> Optional<Individual<T>> run(Collection<T> startPop,
                                           RatingFunction<T> rater,
                                           EnvironmentalSelector<T> environmentalSelector,
                                           Mutator<T> mutator,
                                           Selector<T> parentParentSelector,
                                           Recombinator<T> recombinator,
                                           TerminationCondition<T> condition) throws RatingsExhaustedException {
        Collection<Individual<T>> pop = startPop.stream().map(i -> new Individual<>(i)).collect(toList());
        rateFitness(rater, pop); // We let a possible exception through because the initial population must be rated.
        Optional<Individual<T>> bestIndividual = getBestIndividual(pop);

        int i = 0;
        eventBus.post(new NextPopulationCreated<>(i, pop, bestIndividual.get()));
        while (bestIndividual.isPresent() && !condition.met(bestIndividual.get(), i++)) {
            pop = mutator.mutate(pop);
            try {
                rateFitness(rater, pop);
            } catch (RatingsExhaustedException ignore) {
                return getBestIndividual(pop);
            }

            Collection<Individual<T>> parents = parentParentSelector.select(pop);
            Collection<Individual<T>> offspring = recombinator.recombine(pop);

            try {
                rateFitness(rater, parents);
                rateFitness(rater, offspring);
            } catch (RatingsExhaustedException ignore) {
                Optional<Individual<T>> bestParent = getBestIndividual(parents);
                Optional<Individual<T>> bestOffspring = getBestIndividual(offspring);
                return Optional.of(FITNESS_ORDERING.max(bestParent.get(), bestOffspring.get()));
            }

            pop = environmentalSelector.select(parents, offspring);
            bestIndividual = getBestIndividual(pop);
            eventBus.post(new NextPopulationCreated<>(i, pop, bestIndividual.get()));
        }
        return bestIndividual;
    }

    private <T> Optional<Individual<T>> getBestIndividual(Collection<Individual<T>> individual) {
        return individual.stream().filter(i -> i.getFitness().isPresent()).max(Individual.FITNESS_COMPARATOR);
    }

    private <T> void rateFitness(RatingFunction<T> rater, Collection<Individual<T>> pop) throws
            RatingsExhaustedException {
        for (Individual<T> individual : pop) {
            if (!individual.getFitness().isPresent()) {
                individual.setFitness(rater.rate(individual.getValue()));
            }
        }
    }

    public static class NextPopulationCreated<T> {
        private final int iteration;
        private final Collection<Individual<T>> pop;
        private final Individual<T> best;
        private final double avg;

        public NextPopulationCreated(int iteration, Collection<Individual<T>> pop, Individual<T> best) {
            this.iteration = iteration;
            this.pop = copyOf(pop);
            this.best = best;
            this.avg = pop.stream()
                    .filter(p -> p.getFitness().isPresent())
                    .collect(averagingDouble(p -> p.getFitness().get()));
        }

        public int getIteration() {
            return iteration;
        }

        public Collection<Individual<T>> getPop() {
            return pop;
        }

        public Individual<T> getBest() {
            return best;
        }

        public double getAvg() {
            return avg;
        }
    }

    public static class PopulationCounter {

        private int iteration = 0;

        @Subscribe
        public <T> void nextPopulationCreated(NextPopulationCreated<T> event) {
            iteration = event.getIteration();
        }

        public int totalPopulations() {
            return iteration;
        }
    }
}
