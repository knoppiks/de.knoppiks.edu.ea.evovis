package de.knoppiks.evovis.model;

public interface RatingFunction<T> {

    double rate(T individual) throws RatingsExhaustedException;

    int timesCalled();

    /**
     * The RatingsExhaustedException may be thrown by the RatingFunction to indicate that too many calls to it have been
     * done. This can happen in the following cases:
     * <ul>
     * <li>The rating function is very expensive and we want to restrict calls to it synthetically.</li>
     * <li>Calls to an outer API are limited.</li>
     * <li>We want to restrict calls as part of the termination process.</li>
     * </ul>
     * <p>
     * {@link RatingFunctions}
     */
    public static class RatingsExhaustedException extends Exception {
        private int timesCalled;

        public RatingsExhaustedException(int timesCalled) {
            super(String.format("The rating function must not be called more often than %d.", timesCalled));
            this.timesCalled = timesCalled;
        }

        public int getTimesCalled() {
            return timesCalled;
        }
    }
}
