package de.knoppiks.evovis.model;

import java.util.Collection;

public interface Selector<T> {

    public Collection<Individual<T>> select(Collection<Individual<T>> population);
}
