package de.knoppiks.evovis.model;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class RatingFunctions {

    public static <T> RatingFunction inverse(RatingFunction<T> base) {
        return new RatingFunction<T>() {
            @Override
            public double rate(T individual) throws RatingsExhaustedException {
                return -base.rate(individual);
            }

            @Override
            public int timesCalled() {
                return base.timesCalled();
            }
        };
    }

    public static <T> RatingFunction<T> cached(RatingFunction<T> base) {
        final AtomicInteger c = new AtomicInteger(0);
        final LoadingCache<T, Double> cache = CacheBuilder.newBuilder().build(new CacheLoader<T, Double>() {
            @Override
            public Double load(T key) throws Exception {
                c.incrementAndGet();
                return base.rate(key);
            }
        });
        return new RatingFunction<T>() {
            @Override
            public double rate(T individual) throws RatingsExhaustedException {
                try {
                    return cache.get(individual);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public int timesCalled() {
                return c.get();
            }
        };
    }

    /**
     * Returns an unlimited RatingFunction, which means that no {@link de.knoppiks.evovis.model.RatingFunction
     * .RatingsExhaustedException} can be thrown by it.
     *
     * @param base The real rating function, which evaluates the fitness of an individual.
     * @param <T>  The individual type.
     * @return A RatingFunction never throwing an {@link de.knoppiks.evovis.model.RatingFunction
     * .RatingsExhaustedException} and uses the {@code base} Rater
     * function to evaluate the fitness of an individual.
     */
    public static <T> RatingFunction<T> unlimited(Rater<T> base) {
        return new CountingRatingFunction<>((individual, i) -> base.rate(individual));
    }

    /**
     * Returns a limited RatingFunction, which means that after {@code maxTimes} calls of the rating function a
     * {@link de.knoppiks.evovis.model.RatingFunction.RatingsExhaustedException} will be thrown
     *
     * @param base     The real rating function, which evaluates the fitness of an individual.
     * @param <T>      The individual type.
     * @param maxTimes The maximum number of calls to the underlying base rating function.
     * @return A RatingFunction throwing an {@link de.knoppiks.evovis.model.RatingFunction.RatingsExhaustedException}
     * after {@code maxTimes} calls to the
     * {@code base} Rater function to evaluate the fitness of an individual.
     */
    public static <T> RatingFunction<T> limited(Rater<T> base, int maxTimes) {
        return new CountingRatingFunction<>((individual, timesRated) -> {
            if (timesRated >= maxTimes) throw new RatingFunction.RatingsExhaustedException(timesRated);
            return base.rate(individual);
        });
    }

    public static interface Rater<T> {
        public double rate(T individual);
    }

    public static class CountingRatingFunction<T> implements RatingFunction<T> {

        private final Rater<T> base;

        protected int timesCalled;

        public CountingRatingFunction(Rater<T> base) {
            this.base = base;
            this.timesCalled = 0;
        }

        @Override
        public double rate(T individual) throws RatingsExhaustedException {
            timesCalled++;
            return base.rate(individual, timesCalled);
        }

        @Override
        public int timesCalled() {
            return timesCalled;
        }

        public static interface Rater<T> {
            public double rate(T individual, int timesCalled) throws RatingsExhaustedException;
        }
    }
}
