package de.knoppiks.evovis.model;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Random;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toList;

public abstract class Selectors {
    public static <T> Selector<T> tournamentSelect(Random r, int n, int k) {
        return p -> {
            Set<Individual<T>> selection = newHashSet();
            List<Individual<T>> population = ImmutableList.copyOf(p);
            selection.addAll(Individual.FITNESS_ORDERING.greatestOf(p, k));
            while (selection.size() < n && selection.size() < p.size()) {
                selection.add(Individual.FITNESS_ORDERING.max(
                        population.get(r.nextInt(population.size())),
                        population.get(r.nextInt(population.size()))));
            }
            return selection;
        };
    }

    public static <T> Selector<T> selectBest(int n) {
        return p -> Individual.FITNESS_ORDERING.greatestOf(p, n).stream().collect(toList());
    }

    public static <T> Selector<T> all() {
        return p -> p;
    }
}
