package de.knoppiks.evovis.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.collect.Ordering;

import java.util.Comparator;

import static com.google.common.base.Optional.absent;
import static java.lang.Double.compare;

public class Individual<T> {

    public static final Comparator<Individual<?>> FITNESS_COMPARATOR =
            (Individual<?> a, Individual<?> b) -> compare(a.getFitness().get(), b.getFitness().get());
    public static final Ordering<Individual<?>> FITNESS_ORDERING = Ordering.from(FITNESS_COMPARATOR);

    private final T individual;
    private Optional<Double> fitness;

    public Individual(T individual, double fitness) {
        this.individual = individual;
        this.fitness = Optional.of(fitness);
    }

    public Individual(T individual) {
        this.individual = individual;
        this.fitness = absent();
    }

    public T getValue() {
        return individual;
    }

    public Optional<Double> getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        if (this.fitness.isPresent()) {
            throw new IllegalStateException("Fitness already set.");
        } else {
            this.fitness = Optional.of(fitness);
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("individual", individual)
                .add("fitness", fitness)
                .toString();
    }
}
